all: build install

build:
	xgettext --from-code=UTF-8 --output=ideapad-mode\@annexhack.inceptive.ru/locale/en.pot ./ideapad-mode\@annexhack.inceptive.ru/*.js
	gnome-extensions pack -f --podir=locale ./ideapad-mode\@annexhack.inceptive.ru/ --out-dir=./

install:
	gnome-extensions install --force ./ideapad-mode\@annexhack.inceptive.ru.shell-extension.zip